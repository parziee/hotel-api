# Simple Node REST API for Hotel APP

## Features

* Babel 7
* Environment Variables
* Express
* REST API

## Requirements

* [node & npm](https://nodejs.org/en/)
* [git](https://git-scm.com/)

## Installation

* `git clone git@github.com:parziee/hotel-api.git`
* `cd hotel-api`
* `npm install`
* `npm start`
* optional: include *.env* in your *.gitignore*

### GET Routes

* visit http://localhost:3000
  * /hotels
  * /hotels/:id
  * /hotels/filterBy/:name/:stars

#### Postman

* Install [Postman](https://www.getpostman.com/apps) to interact with REST API
* Create a message with:
  * URL: http://localhost:3000/hotels
  * Method: POST
  * Body: raw + JSON (application/json)
  * Body Content: `   {
        "name": "Hotel Lucho",
        "stars": 3,
        "price": 182,
        "image": "ccc.jpg",
        "amenities": [
            "newspaper",
            "beach-pool-facilities",
            "beach",
            "garden",
            "fitness-center"
        ]
    }
`
* Delete a message with:
  * URL: http://localhost:3000/hotels/1
  * Method: DELETE
