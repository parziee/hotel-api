import session from './session';
import user from './user';
import hotel from './hotels';

export default {
  session,
  user,
  hotel,
};
