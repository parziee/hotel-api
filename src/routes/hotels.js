import uuidv4 from 'uuid/v4';
import {
  Router
} from 'express';

const router = Router();

router.get('/', (req, res) => {
  return res.send(Object.values(req.context.models.hotels));
});

router.get('/filterBy/:name/:stars', (req, res) => {

  const name = req.params.name;
  const stars = req.params.stars;
  const hotels = req.context.models.hotels;

  const filteredHotels = hotels.filter(hotel => {
    if (stars.includes(hotel.stars) && hotel.name.toLowerCase().includes(name.toLowerCase())) {
      return hotel;
    }
  });
  return res.send(filteredHotels);
});

router.get('/:hotelId', (req, res) => {

  const hotel = req.context.models.hotels.filter(hotel => hotel.id === req.params.hotelId);
  return res.send(hotel);
});

router.post('/', (req, res) => {

  const id = uuidv4();
  const hotel = {
    id,
    name: req.body.name,
    stars: req.body.stars,
    price: req.body.price,
    image: req.body.image,
    amenities: req.body.amenities
  };

  req.context.models.hotels.push(hotel);

  return res.send(hotel);
});

router.delete('/:hotelId', (req, res) => {

  const filteredHotels = req.context.models.hotels.filter(hotel => hotel.id !== req.params.hotelId);
  req.context.models.hotels = filteredHotels;

  return res.send(filteredHotels);
});

export default router;